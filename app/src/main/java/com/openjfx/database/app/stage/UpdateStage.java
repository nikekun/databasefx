package com.openjfx.database.app.stage;

import com.openjfx.database.app.BaseStage;
import com.openjfx.database.app.annotation.Layout;
import javafx.stage.StageStyle;

/**
 * application update stage
 *
 * @author yangkui
 * @since 1.0
 */
@Layout(layout = "update_view.fxml", width = 400, height = 500, stageStyle = StageStyle.UNDECORATED)
public class UpdateStage extends BaseStage<Void> {

}
